#! /usr/bin/python
#
# Build the entire ISAAC Project

import subprocess
import os
import sys
import argparse
parser = argparse.ArgumentParser(description='Build entire ISAAC Project Suite')
parser.add_argument('-s', '--skipTests', action='store_true',
					help='Skip the Maven Tests by executing the install command with the -DskipTests flag')
parser.add_argument('-U', '--U', action='store_true',
					help='Update maven dependencies')

projects = ['ISAAC',
			'ISAAC-term-convert-cpt',
			'ISAAC-term-convert-cvx',
			'ISAAC-term-convert-nucc',
			'ISAAC-term-convert-hl7v3',
			'ISAAC-term-convert-rf2',
			'ISAAC-term-convert-vhat',
			'ISAAC-term-convert-loinc',
			'ISAAC-term-convert-rxnorm',
#			'ISAAC-db-builder-solor',
#			'ISAAC-db-builder-vhat',
#			'ISAAC-db-builder-vets',
			'ISAAC-fx-gui',
			'ISAAC-fx-gui-pa',
			'ISAAC-rest']

cliArgs = parser.parse_args()
defaultArgs = ['-e', 'clean']
shellVar=False

if (os.name == 'nt'):
	shellVar=True

def mvn(args):
	return subprocess.check_call(['mvn'] + args, shell=shellVar)


for project in projects:
	cwd = os.getcwd()
	print("In: " + cwd + " Entering project " + project)
	os.chdir(project)

	args = defaultArgs[:]
	if project == 'ISAAC-fx-gui-pa':
		args.extend(['package'])
	else:
		args.extend(['install'])
		
	if cliArgs.skipTests:
		args.extend(['-DskipTests'])
			
	if cliArgs.U:
		args.extend(['-U'])

	print ("Build Argument")
	print (args)
	
	if (os.path.isfile('pom.xml')):
		#This fails the build, if it results in a non-0 exit status
		mvn(args)
	else:
		for folder in os.listdir(os.curdir):
			if (os.path.abspath(folder).endswith('mojo')):
				os.chdir(folder)
				#This fails the build, if it results in a non-0 exit status
				mvn(args)
				os.chdir(os.pardir)
	os.chdir(os.pardir)