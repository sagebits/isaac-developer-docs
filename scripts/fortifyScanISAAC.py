#! /usr/bin/python
#
# Build the entire ISAAC Project

import subprocess
import os
import sys
import argparse
parser = argparse.ArgumentParser(description='Run HP Fortify on entire ISAAC suite')


projects = ['ISAAC',
			'ISAAC-term-convert-cpt',
			'ISAAC-term-convert-cvx',
			'ISAAC-term-convert-nucc',
			'ISAAC-term-convert-hl7v3',
			'ISAAC-term-convert-rf2',
			'ISAAC-term-convert-vhat',
			'ISAAC-term-convert-loinc',
			'ISAAC-term-convert-rxnorm',
			'ISAAC-db-builder-solor',
			'ISAAC-db-builder-vhat',
			'ISAAC-db-builder-vets',
#			'ISAAC-fx-gui',
#			'ISAAC-fx-gui-pa',
			'ISAAC-rest',
#			'ISAAC-file-transfer-utils']

cliArgs = parser.parse_args()
defaultArgs = []
shellVar=False

if (os.name == 'nt'):
	shellVar=True

def mvn(args):
	return subprocess.check_call(['mvn'] + args, shell=shellVar)

# $ mvn -Dmaven.test.skip=true -Dfortify.sca.buildId={PROJECT_NAME} -Dfortify.sca.toplevel.artifactId=isaac-parent com.hpe.security.fortify.maven.plugin:sca-maven-plugin:clean
# $ mvn -Dmaven.test.skip=true -Dfortify.sca.buildId={PROJECT_NAME} -Dfortify.sca.toplevel.artifactId=isaac-parent com.hpe.security.fortify.maven.plugin:sca-maven-plugin:translate
# $ mvn -Dmaven.test.skip=true -Dfortify.sca.buildId={PROJECT_NAME} -Dfortify.sca.toplevel.artifactId=isaac-parent com.hpe.security.fortify.maven.plugin:sca-maven-plugin:scan

fortifyCmds = ['clean', 'translate', 'scan']

for project in projects:
	for fortifyCmd in fortifyCmds:
		cwd = os.getcwd()
		print("In: " + cwd + " Entering project " + project)
		os.chdir(project)
			
		args = defaultArgs[:]
		
#   		if cliArgs.skipTests:
#	   	args.extend(['-DskipTests'])
		args.extend(['-Dmaven.test.skip=true'])
		args.extend(['-Dfortify.sca.buildId=' + project])
		args.extend(['-Dfortify.sca.toplevel.artifactId=isaac-parent'])
		args.extend(['com.hpe.security.fortify.maven.plugin:sca-maven-plugin:' + fortifyCmd])

		print ("Build Argument")
		print (args)

		if (os.path.isfile('pom.xml')):
			#This fails the build, if it results in a non-0 exit status
			mvn(args)
		else:
			for folder in os.listdir(os.curdir):
				if (os.path.abspath(folder).endswith('mojo')):
					os.chdir(folder)
					#This fails the build, if it results in a non-0 exit status
					mvn(args)
					os.chdir(os.pardir)
		os.chdir(os.pardir)
