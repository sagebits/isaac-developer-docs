#! /usr/bin/python
#
# This script clones each project from GIT
#
#  Utilize something like this, ahead of time, to cache https credentials...
#  git config --global credential.helper cache
#  git config --global credential.https://vadev.mantech.com darmbrust
#
import subprocess
import os

projects = ['aitc_install',
			'apache_extensions',
			'ISAAC-db-builder-solor',
			'ISAAC-db-builder-vhat',
			'ISAAC-db-builder-vets',
			'ISAAC-developer-docs',
			'ISAAC-file-transfer-utils',
			'ISAAC-fx-gui-pa',
			'ISAAC-fx-gui',
			'ISAAC-rest',
			'ISAAC-term-convert-cpt',
			'ISAAC-term-convert-cvx',
			'ISAAC-term-convert-hl7v3',
			'ISAAC-term-convert-icd10',
			'ISAAC-term-convert-loinc',
			'ISAAC-term-convert-mvx',
			'ISAAC-term-convert-nucc',
			'ISAAC-term-convert-rf2',
			'ISAAC-term-convert-rxnorm',
			'ISAAC-term-convert-sopt',
			'ISAAC-term-convert-vhat',
			'ISAAC',
			'ntrt-jira-ssoi-authenticator',
			'rails_common',
			'rails_komet',
			'rails_komet.wiki',
			'rails_prisme',
			'VUID-rest']

def git(*args):
	return subprocess.check_call(['git'] + list(args))

gitHubURL = 'https://vadev.mantech.com:4848/git/r/'
branch = 'develop'

for project in projects:
		if os.path.isdir(os.getcwd() + os.sep + project)== False:
				print(os.getcwd() + os.sep + project + " does not exist")
				thisProjectUrl = gitHubURL + project + '.git'
				print("==================================")
				print("Cloning Latest From " + project)
				
				tmpBranch = ''
				if project == 'ISAAC-developer-docs':
					tmpBranch = 'master'
				else:
					tmpBranch = branch
				
				git("clone", "--branch", tmpBranch, thisProjectUrl)
				print("==================================")
				print("");
		else:
				print(project + " already exists in " + os.getcwd())
