#! /usr/bin/python
#
# Utility command to mirror all (local) repositories to external
#
#  Utilize something like this, ahead of time, to cache https credentials...
#  git config --global credential.helper cache
#  git config --global credential.https://vadev.mantech.com darmbrust

import subprocess


projects = ['aitc_install',
			'apache_extensions',
			'ISAAC-db-builder-solor',
			'ISAAC-db-builder-vhat',
			'ISAAC-db-builder-vets',
			'ISAAC-developer-docs',
			'ISAAC-file-transfer-utils',
			'ISAAC-fx-gui-pa',
			'ISAAC-fx-gui',
			'ISAAC-rest',
			'ISAAC-term-convert-cpt',
			'ISAAC-term-convert-cvx',
			'ISAAC-term-convert-hl7v3',
			'ISAAC-term-convert-icd10',
			'ISAAC-term-convert-loinc',
			'ISAAC-term-convert-mvx',
			'ISAAC-term-convert-nucc',
			'ISAAC-term-convert-rf2',
			'ISAAC-term-convert-rxnorm',
			'ISAAC-term-convert-sopt',
			'ISAAC-term-convert-vhat',
			'ISAAC',
			'ntrt-jira-ssoi-authenticator',
			'rails_common',
			'rails_komet',
			'rails_komet.wiki',
			'rails_prisme',
			'VUID-rest']

def git(*args):
	return subprocess.check_call(['git'] + list(args))

#remoteBaseURL = 'https://daniel_armbrust@bitbucket.org/daniel_armbrust/'
#remoteBaseURL = 'https://darmbrust@vadev.mantech.com:4848/git/r/'
remoteBaseURL = 'https://devtest@vaauscttdbs80.aac.va.gov:8080/git/r/'

for project in projects:
	print("==================================")
	print("Tag/Branch Push " + project)
	git("-c", "http.sslVerify=false", "-C", project + ".git", "push", "--all",  remoteBaseURL + project + ".git")
	git("-c", "http.sslVerify=false", "-C", project + ".git", "push", "--tags",  remoteBaseURL + project + ".git")
	print("==================================")
	print("");
