#! /usr/bin/python
#
# Utility command to add a remote to all repositories
#
#  Utilize something like this, ahead of time, to cache https credentials...
#  git config --global credential.helper cache
#  git config --global credential.https://vadev.mantech.com darmbrust

import subprocess


projects = ['aitc_install',
			'apache_extensions',
			'ISAAC-db-builder-solor',
			'ISAAC-db-builder-vhat',
			'ISAAC-db-builder-vets',
			'ISAAC-developer-docs',
			'ISAAC-file-transfer-utils',
			'ISAAC-fx-gui-pa',
			'ISAAC-fx-gui',
			'ISAAC-rest',
			'ISAAC-term-convert-cpt',
			'ISAAC-term-convert-cvx',
			'ISAAC-term-convert-hl7v3',
			'ISAAC-term-convert-icd10',
			'ISAAC-term-convert-loinc',
			'ISAAC-term-convert-mvx',
			'ISAAC-term-convert-nucc',
			'ISAAC-term-convert-rf2',
			'ISAAC-term-convert-rxnorm',
			'ISAAC-term-convert-sopt',
			'ISAAC-term-convert-vhat',
			'ISAAC',
			'ntrt-jira-ssoi-authenticator',
			'rails_common',
			'rails_komet',
			'rails_komet.wiki',
			'rails_prisme',
			'VUID-rest']

def git(*args):
	return subprocess.check_call(['git'] + list(args))

remoteName = 'origin'
remoteBaseURL = 'https://vadev.mantech.com:4848/git/r/'

for project in projects:
	print("==================================")
	print("Updating Remote of " + project + " current:")
	git("-C", project, "remote", "-v")
	git("-C", project, "remote", "set-url", remoteName, remoteBaseURL + project + ".git")
	print("New:")
	git("-C", project, "remote", "-v")
	print("==================================")
	print("");
