#! /usr/bin/python
#
# Various git commands that Dan uses to fetch from two different remotes, and then push code from one to the other.
#
# Note that this script does NOT push code from your local development or master branch anywhere.  It only 
# pushes code that has already been pushed to remote1/develop to remote2/develop, and remote1/master to remote2/master.
# Note that is will push any local tags, however.  
# This code also only pushes develop and master - no other branches are handled.
#
#  Utilize something like this, ahead of time, to cache https credentials...
#  git config --global credential.helper cache
#  git config --global credential.https://vadev.mantech.com darmbrust

import subprocess


projects = ['aitc_install',
			'apache_extensions',
			'ISAAC-db-builder-solor',
			'ISAAC-db-builder-vhat',
			'ISAAC-db-builder-vets',
			'ISAAC-developer-docs',
			'ISAAC-file-transfer-utils',
			'ISAAC-fx-gui-pa',
			'ISAAC-fx-gui',
			'ISAAC-rest',
			'ISAAC-term-convert-cpt',
			'ISAAC-term-convert-cvx',
			'ISAAC-term-convert-hl7v3',
			'ISAAC-term-convert-icd10',
			'ISAAC-term-convert-loinc',
			'ISAAC-term-convert-mvx',
			'ISAAC-term-convert-nucc',
			'ISAAC-term-convert-rf2',
			'ISAAC-term-convert-rxnorm',
			'ISAAC-term-convert-sopt',
			'ISAAC-term-convert-vhat',
			'ISAAC',
			'ntrt-jira-ssoi-authenticator',
			'rails_common',
			'rails_komet',
			'rails_komet.wiki',
			'rails_prisme',
			'VUID-rest']

def git(*args):
	return subprocess.check_call(['git'] + list(args))

remoteMaster = 'origin'
remoteSlave = 'vadev'

for project in projects:
	print("==================================")
	print("Fetching Latest from " + project)
	git("-C", project, "fetch", remoteMaster, "--tags")
	git("-C", project, "fetch", remoteSlave)
	
	if project != 'ISAAC-developer-docs':
		print("Pushing develop branch from " + remoteMaster + " to " + remoteSlave)
		git("-C", project, "push", remoteSlave, remoteMaster + "/develop:refs/heads/develop")

	print("Pushing master branch from " + remoteMaster + " to " + remoteSlave)
	git("-C", project, "push", remoteSlave, remoteMaster + "/master:refs/heads/master")

	print("Pushing Tags to " + remoteSlave)
	git("-C", project, "push", remoteSlave, "--tags")
	print("==================================")
print("");
