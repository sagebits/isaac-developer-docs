#! /usr/bin/python
#
# Utility to remove a remote from all repositories
#
#
#
#

import subprocess


projects = ['aitc_install',
			'apache_extensions',
			'ISAAC-db-builder-solor',
			'ISAAC-db-builder-vhat',
			'ISAAC-db-builder-vets',
			'ISAAC-developer-docs',
			'ISAAC-file-transfer-utils',
			'ISAAC-fx-gui-pa',
			'ISAAC-fx-gui',
			'ISAAC-rest',
			'ISAAC-term-convert-cpt',
			'ISAAC-term-convert-cvx',
			'ISAAC-term-convert-hl7v3',
			'ISAAC-term-convert-icd10',
			'ISAAC-term-convert-loinc',
			'ISAAC-term-convert-mvx',
			'ISAAC-term-convert-nucc',
			'ISAAC-term-convert-rf2',
			'ISAAC-term-convert-rxnorm',
			'ISAAC-term-convert-sopt',
			'ISAAC-term-convert-vhat',
			'ISAAC',
			'ntrt-jira-ssoi-authenticator',
			'rails_common',
			'rails_komet',
			'rails_komet.wiki',
			'rails_prisme',
			'VUID-rest']

def git(*args):
	return subprocess.check_call(['git'] + list(args))

remoteName = 'origin'

for project in projects:
	print("==================================")
	print("Removing Remote from " + project)
	git("-C", project, "remote", "remove", remoteName)
	git("-C", project, "remote", "-v")
	print("==================================")
	print("");
