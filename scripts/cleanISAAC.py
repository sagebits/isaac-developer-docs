#! /usr/bin/python
#
# Python script to go to each ISAAC project folder and execute a maven clean
#
#
#

import subprocess
import os

#

projects = ['ISAAC',
			'ISAAC-term-convert-cpt',
			'ISAAC-term-convert-cvx',
			'ISAAC-term-convert-nucc',
			'ISAAC-term-convert-hl7v3',
			'ISAAC-term-convert-rf2',
			'ISAAC-term-convert-vhat',
			'ISAAC-term-convert-loinc',
			'ISAAC-term-convert-rxnorm',
			'ISAAC-db-builder-solor',
			'ISAAC-db-builder-vhat',
			'ISAAC-db-builder-vets',
			'ISAAC-fx-gui',
			'ISAAC-fx-gui-pa',
			'ISAAC-rest']

shellVar=False

if (os.name == 'nt'):
	shellVar=True

def mvn(*args):
	return subprocess.check_call(['mvn'] + list(args), shell=shellVar)

def cleanPath():
	if (os.path.isfile('pom.xml')):
		print("Cleaning " + os.path.abspath('pom.xml'))
		mvn("clean")
	else:
		for folder in os.listdir(os.curdir):
			if (os.path.isdir(folder)):
				os.chdir(folder)
				cleanPath()
				os.chdir("..")
	return

for project in projects:
	if os.path.isdir(os.getcwd() + os.sep + project):
		os.chdir(project)
		cleanPath()
		os.chdir("..")
	else:
		print(project + " does not exist!")