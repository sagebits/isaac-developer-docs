# Database Source processing

The following projects are used to process native source content into the form used by ISAAC.

## Projects
- ISAAC-term-convert-rf2 - https://vadev.mantech.com:4848/git/summary/ISAAC-term-convert-rf2
  - Converts RF2 formats (SNOMED, Extensions) from native to an ISAAC form suitable for database building
- ISAAC-term-convert-loinc - https://vadev.mantech.com:4848/git/summary/ISAAC-term-convert-loinc
  - Converts LOINC from native to an ISAAC form suitable for database building.
- ISAAC-term-convert-rxnorm - https://vadev.mantech.com:4848/git/summary/ISAAC-term-convert-rxnorm
  - Converts RxNorm from native to an ISAAC form suitable for database building.  Not yet working with the ISAAC refactor.
  
  
## Per-Project Details
Each of the projects above follows the pattern:

- [type]-src-data
  - Used for uploading the native source format, exactly as it was released, into the maven artifact repository.  See the included 
  readme file for instructions on use.  No tagging / releasing is done based on work in this folder - it is only used as way to upload
  the source content as an artifact with the correct format and version information.  Some projects, such as the rf2 converter, have a 
  hierarchy under this folder, to handle more that one data release of the same format - Snomed International, and US Extension, for example.
- [term]-mojo
  - The code that does the conversion from native source format into iblf, suitable for ISAAC.  This code is released and managed 
  via gitflow.  See the included ReadMe.md file for details on doing a release. 
- [term]-ibdf
  - Contains a configuration that executes the mojo above, on the source uploaded further above.  The resulting artifact is the ibdf file
  containing content suitable for loading into ISAAC via a Database Builder such as ISAAC-db-builder-solor.  No tagging / releasing is done 
  based on work in this folder - it is only used as a way to execute the conversion, and upload the resulting artifact with the correct format 
  and version information.  See the included ReadMe.md for details on deploying the resulting artifact.   Some projects, such as the rf2 converter,
  have a hierarchy under this folder, to handle more that one data release of the same format - Snomed International, and US Extension, for example.
  
## Conversion Output
- Each output file is a zip file - however the type when requesting them in maven in "ibdf.zip", rather than just "zip".
- Each zip file contains:
  - The ibdf artifact
  - a META-INF folder
    - License / Manifest / Notice files 
    - **maven** folder
      - The maven folder contains the entire maven structure necessary to reproduce this _exact_ artifact.  The means that is provides a complete
      record of all of the software and content artifacts that were used to produce the ibdf file.