#KOMET setup

To run an instance of KOMET locally, you need a Tomcat 8 server to deploy the war file into.

You also need an ISAAC-Rest server - which can either be the war file deployed into Tomcat, or run from Jetty via a console launcher.

##ISAAC-Rest

- First clone https://vadev.mantech.com:4848/git/r/ISAAC-rest.git

- To launch with the local jetty server, execute this class (which is found in the test source tree): gov.vha.isaac.rest.LocalJettyRunner

- If you do nothing to configure a database, it will automatically download a recent database from https://vadev.mantech.com:8080/nexus/content/groups/everything/
and extract it into your temp folder, and then launch the server.  When the server comes up, an example URL will be printed on the console.

- To launch with your own local database, specify the following JVM parameter (with the appropriate edits):
-DisaacDatabaseLocation=/mnt/STORAGE/Work/VetsEZ/Workspaces/ISAAC2/ISAAC-rest/vets-1.2-SNAPSHOT-all.data/

##KOMET

- To build your own copy:

  - You must have a maven configuration that points to the vadev nexus server: https://raw.githubusercontent.com/VA-CTT/ISAAC-developer-docs/master/resources/settings.xml
  
  - You must edit the user name and password in your new settings.xml file (look for this):<br>
  ```
			<username>***USERNAME***</username>
			<password>***PASSWORD***</password>
  ```

  - First clone https://vadev.mantech.com:4848/git/r/rails_komet.git

  - run 'mvn clean package'
  
  - locate the war in target/

- Alternatively, download a war from: https://vadev.mantech.com:8080/nexus/content/groups/public/gov/vha/isaac/gui/rails/rails_komet/

You may use either the '*_a' or '*_b' war - they are identical.

Deploy the war file into your local Tomcat 8 server.

Browse to the deployed KOMET application - it should come up successfully serving data from the local isaac rest server.  You may log in with devtest/devtest
