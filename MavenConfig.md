# Maven Configuration

We have a Nexus Artifact server at https://vadev.mantech.com:8080/nexus/ for development purposes.

## Maven Settings
A maven settings file to be placed in your .m2 folder (in your home directory) [settings.xml] is available either 
a) online: ( https://raw.githubusercontent.com/VA-CTT/ISAAC-developer-docs/master/resources/settings.xml)
b) in this project: ISAAC-developer-docs/resources/settings.xml

Replace the \*\*\*USERNAME\*\*\* and \*\*\*PASSWORD\*\*\* fields with the appropriate values.

This settings file will redirect all of your maven access through the development nexus artifact server.
It also specifies the credentials necessary for accessing restricted artifacts, and for deploying artifacts.

## Repositories
The repository server  https://vadev.mantech.com:8080/nexus/#view-repositories has 3 public repositories:
 - Releases (releases)
 - Snapshots (snapshots)
 - Central (central)

The content in these can be accessed by individual URLs, or by the group:
https://vadev.mantech.com:8080/nexus/content/groups/public/

The following repositories are only available to users with credentials:
 - Terminology Data (termdata)
 - Terminology Data Snapshots (termdatasnapshots)

Developers with credentials should use the URL https://vadev.mantech.com:8080/nexus/content/groups/everything/
to access the contents of all 5 repositories at the same time.

A maven build command for deploying a snapshot build of open code would look like this:

``
mvn clean deploy -DaltDeploymentRepository=vadev::default::https://vadev.mantech.com:8080/nexus/content/repositories/snapshots/
``

A maven build command for deploying (and tagging) open code would look like this:

``
mvn jgitflow:release-start jgitflow:release-finish -DreleaseVersion=3.01 -DdevelopmentVersion=3.02-SNAPSHOT -DaltDeploymentRepository=vadev::default::https://vadev.mantech.com:8080/nexus/content/repositories/releases/
``

But any content releases (built databases, ibdf files, source content) should use the **termdata** repository: 

``
-DaltDeploymentRepository=vadev::default::https://vadev.mantech.com:8080/nexus/content/repositories/termdata/
``

In the short term, while testing things, we can also use this SNAPSHOT repository for terminology data:

``
-DaltDeploymentRepository=vadev::default::https://vadev.mantech.com:8080/nexus/content/repositories/termdatasnapshots/
``

But in the future, we shouldn't have a need for SNAPSHOT builds of content.