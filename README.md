# ISAAC-developer-docs
A repository for storing basic developer oriented documentation about the suite of ISAAC projects

Note that the purpose of this repository is not to provide design documentation for ISAAC.  Rather, this project is 
intended to be a simple, concise set of documentation that is geared toward developers that are trying to build
the ISAAC suite of software.

If you need a summary of the types of changes that have occurred with the migration away from the OTF APIs, see 
https://github.com/VA-CTT/ISAAC-developer-docs/blob/master/OTFMigration.md

##Running Komet Locally
See https://github.com/VA-CTT/ISAAC-developer-docs/blob/master/Komet.md

##Requirements
ISAAC requires modern supporting software - this means Java 8, and Maven 3.x or newer.  

##Projects
Currently, there are 6 related projects - each with its own repository - each independently versioned.  Note, the order here is also the order
the projects need to be built in, if you want to build the entire code stack.

- ISAAC-developer-docs - https://github.com/VA-CTT/ISAAC-developer-docs.git
  - This project - developer documentation
- ISAAC - https://vadev.mantech.com:4848/git/summary/ISAAC.git
  - Lowest level ISAAC code.  Contains all APIs, and implementations - but no GUI code.  
- ISAAC-term-convert-rf2 - https://vadev.mantech.com:4848/git/summary/ISAAC-term-convert-rf2.git
  - Code built on top of ISAAC that enables the translation of RF2 formatted content into the native ISAAC format.  Note that this project
    is not typically part of an ISAAC end user application - this is only used to translate RF2 native format into ISAAC native format.
- ISAAC-db-builder-solor - https://vadev.mantech.com:4848/git/summary/ISAAC-db-builder-solor.git
  - An example project that takes in a collection of natively formatted ISAAC data files, and builds a database out of them.
    This particular example combines the ISAAC base metadata and SnomedCT.  Note that this project is not typically part of an ISAAC end 
    user application - this is only used to create an ISAAC database out of a series of ISAAC native formated files.
- ISAAC-fx-gui - https://vadev.mantech.com:4848/git/summary/ISAAC-fx-gui.git
  - A JavaFX GUI for ISAAC
- ISAAC-fx-gui-pa - https://vadev.mantech.com:4848/git/summary/ISAAC-fx-gui-pa.git
  - An example *Assembly* project that shows how to create an end-user application out of ISAAC, a database, and the ISAAC-fx-gui.
    While this project contains no code, it contains the configuration for the assembly (things like users, change set repository location, etc)
    and the dependency set for the GUI (which GUI components should be included in the application).  It also contains the configuration for which
    database should be packaged with the GUI, and the installer configuration.  The end result of building this project is a GUI application for
    the end user - which includes all necessary components from the stack above.


The GitHub repositories are the primary 'work' repositories for development.  
**This is where developers should push their work.**

##Maven Build Mangement
Each project is managed and built by Maven.  Most projects are multi-module projects themselves, having multiple modules.  

In general, the necessary external dependencies to build the ISAAC suite should all be available on public repositories.  However, the terminology 
files used by the server are not publicly available.  

See also **[artifact server](https://github.com/VA-CTT/ISAAC-developer-docs/tree/master/MavenConfig.md)**


##Utility Scripts
A few utility scripts are available in the **[scripts](https://github.com/VA-CTT/ISAAC-developer-docs/tree/master/scripts)** sub-folder.  The scripts published
so far are all [python](https://www.python.org/) scripts which aid in management of all of the projects that make up the ISAAC suite.

To use these scripts, copy them into the same folder that contains all of your checked out ISAAC projects (likely, the parent folder of this project)

- cloneAll.py - Executes **git clone --branch 'branchname'** to pull down all of the project code from GitHub

- buildISAAC.py - Executes **mvn -e clean install** or **mvn -e clean package** on each project *in the correct order* to effect a complete build of the entire
ISAAC suite.

- cleanISAAC.py - Executes **mvn clean** on each project to recover disk space

- fetchAll.py - Executes **git fetch** on each project
  

##Development Notes
Developers that are focusing on ISAAC development (and not handling content import) typically only need:
- ISAAC
- ISAAC-fx-gui
- ISAAC-fx-gui-pa


##Eclipse Configuration Notes
Eclipse 4.4 or newer is required for Java 8 support.  Eclipse 4.5 or newer is highly recommended.

**It is HIGHLY recommended that you build on the command line with just maven before trying to bring these projects into Eclipse**

Eclipse [M2E](http://eclipse.org/m2e/) supports most aspects of our maven configuration files.

###Importing Projects
The default integration of Git into eclipse does not clone into the workspace folder.  It is easier to work with the projects if you change this (if you are cloning from within eclipse).

In Window -> Preferences -> Team -> Git -> Default repository folder: - change that to your workspace location or **${workspace_loc}**

Then:

- Clone the projects you want to work on with Git - do not import into the workspace yet.
- Import -> Maven -> Existing Maven Projects
  - Select the project that you checked out
 
Use the "Project Explorer" (not the "Package Explorer") and from the 'Triangle' drop down menu, change the 'Project Presentation' to 'Hierarchial'.

Some of the project also have simple eclipse projects, where there isn't a top-level pom.xml file.  To make the hierarchy work properly in eclipse, 
also do an import of all General -> Existing Projects.


There is a bug in the current version of eclipse - which will cause build 'archiver' errors on some projects.  To resolve this, add this update site:
http://repo1.maven.org/maven2/.m2e/connectors/m2eclipse-mavenarchiver/0.17.2/N/LATEST/

And install the plugin from that update site.

###M2E Lifecycle issues
You should install any M2E plugins that are available to handle lifecycle configurations, so that things such as JaxB code generation work 
correctly.  The pom files already contain sections that should prevent errors for lifecycle issues that m2e cannot handle.


###Eclipse and jaxb2
The jaxb2 plugin for Eclipse does not work properly if your eclipse was launched using a JRE.

If you get an error like this:

```
Execution default of goal org.jvnet.jaxb2.maven2:maven-jaxb2-plugin:0.12.3:generate failed: A required class was 
missing while executing org.jvnet.jaxb2.maven2:maven-jaxb2-plugin:0.12.3:generate: com/sun/xml/bind/api/ErrorListener
```

You need to reconfigure eclipse to launch with a JDK.  The easiest way to do this is to edit the file **eclipse.ini**.  Add a **-vm** parameter, as shown here:

```
-startup
plugins/org.eclipse.equinox.launcher_1.3.0.v20140415-2008.jar
-vm
C:\Program Files\Java\jdk1.8.0_65\bin\javaw.exe
--launcher.library
plugins/org.eclipse.equinox.launcher.win32.win32.x86_64_1.1.200.v20150204-1316
-product
```

###Maven Workspace Resolution
Sometimes when you import projects into Maven, it imports them with "Workspace Resolution" disabled.  This can lead to erroneous errors about not being able to resolve
other modules which are present in the same eclipse workspace.  Make sure Maven -> Enable Workspace Resolution is toggled.  To bulk update, enable workspace resolution, 
then delete, and reimport all of the projects as maven projects.

###Eclipse and JavaFX warnings

Eclipse currently has bugs in dealing with JavaFX code - and will produce copious warnings on classes that involve JavaFX libraries.  This issue can be 
fixed temporarily as documented here:  https://bugs.eclipse.org/bugs/show_bug.cgi?id=431067#c9

Alternatively, install the e(fx)clipse IDE plugins for eclipse.

###Launching ISAAC from Eclipse
Launching ISAAC from within Eclipse is most easily done by creating a run configuration in the **fx-gui-assembly** project.

The main method is **gov.va.isaac.gui.ISAACFxGUI**.  Eclipse sometimes doesn't read the maven classpath properly when configuring a run configuration.  If this
 happens, simply add **most** of the ISAAC projects on the classpath tab of the Run Configuration.

**What to skip:** To make sure the logging configuration comes up properly, do **NOT** include the project **ochre-mojo-log-config**.  Additionally, make 
sure that the project **fx-gui-config** is the first project in your classpath resolution order.  This will ensure that log4j finds the correct configuration file.

Finally, you need a datastore.  Upon startup, ISAAC will try to locate the DB in the folder the JVM was launched from.  It is typically easiest to 
place a copy of the ISAAC datastore within the ISAAC-fx-gui-pa/fx-gui-assembly folder - resulting in this hierarchy:

```
ISAAC-fx-gui-pa
  - fx-gui-assembly
    - snomed-20150731-1.0-SNAPSHOT-all.data
      + META-INF
      + object-chronicles
      + search
```

You can obtain a datastore using one of the following options:
 - **mvn clean package** the project ISAAC-fx-gui-pa using maven.  When the build completes, a datastore will be located in the 
   ISAAC-fx-gui-pa\fx-gui-assembly\target folder.  Move this datastore folder up one folder.
 - Build a datastore using a project like ISAAC-db-builder-solor  - move the resulting datastore from the ISAAC-db-builder-solor *target* subfolder.
 

##Required HEAP size for running with a DB
ISAAC currently requires at least 5 GB of java HEAP to run with the full SNOMED database.  To run with the full SOLOR database, you need at least
6 GB of HEAP.  Unless your system has more than 24 GB of RAM, the default JVM max for the HEAP will be inadequate - as the JVM selects 1/4 of your 
RAM as the default max.  

Furthermore, two other options (below) further improve performance.

The recommended configuration to pass into the JVM launch is:

```
-Xmx6g -XX:+UseG1GC -XX:MetaspaceSize=100M
```

Setting a larger HEAP size is encouraged, if you have a proper development system :)

The G1GC setting enables a newer garbage collector that does a better job at preventing long pauses in the GUI.  The MetaspaceSize parameter is required
to prevent a long pause during the initial usage of the GUI.  These parameters are automatically set when an end user installs ISAAC, and are included in 
the command-line launch scripts.  It is only when running within an IDE environment that you need to manually specify them.

##Logging Configuration
The logging system is configured by the file **ISAAC-fx-gui-pa/fx-gui-config/src/main/resources/log4j2.xml**.  By default, two log files are produced - 
isaac.log and isaac-debug.log.  These will appear in the folder where the JVM was launched from (typically ISAAC-fx-gui-pa/fx-gui-assembly).

To configure so that debug or info level logging appears on the console for development purposes, uncomment the following lines in the log4j2.xml file:

```
    <!--AppenderRef ref="STDOUT-DEBUG" /-->
    <!--AppenderRef ref="STDOUT-INFO" /-->
```

##JavaFX and Windows 10
JavaFX doesn't work properly on Windows 10 with hardware rendering - at least not on my hardware.  Oracle / JavaFX developers are tone-deaf to the problem.

Specify the following VM argument to disable the hardware rending pipeline:

```
-Dprism.order=sw
```
this will allow you to use JavaFX